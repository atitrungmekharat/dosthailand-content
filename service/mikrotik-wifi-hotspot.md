---
title: "Mikrotik Wifi Hotspot"
date: 2019-07-06T01:12:05+07:00
image: images/service/service-1.jpg
bgImage: images/background/page-title.jpg
icon: ti-bar-chart
brochureURL: '#'
regularDay: Sun-Tues
regularTime: 08.00am - 06.00pm
halfDay: Thursday
halfTime: 08.00am - 01.00pm
offDay: Friday
type : service
# draft: true
---


# ติดตั้งระบบ WiFi Hotspot

เรารับติดตั้งและวางระบบ WiFi Hotspot ให้กับหอพัก หรืออพาร์ทเม้นทั่วไป ทั้งเข้าไปติดตั้งเพิ่มเติมจากที่มีอยู่หรือติดตั้งใหม่ทั้งหมดด้วยระบบที่ เป็นมืออาชีพ มี feature ให้ครบทั้งออกคูปองรายวัน รายเดือน ระบบสมาชิกถาวรจนกว่าจะย้ายออก หรือระบบซื้อคูปองออนไลน์ (เฉพาะการติดตั้งแบบร่วมลงทุน)

ในการติดตั้งมีการทดสอบระยะครอบคลุมของสัญญาณให้ลูกค้ารับทราบว่าส่วนใดของ หอพัก อพาร์ทเม้นท์บ้างที่สัญญาณอ่อน หรือไม่ครอบคลุม และลูกค้าสามารถตัดสินใจได้ว่าจะติดเพิ่มเติมหรือไม่
